print ("Guitar Chord Progression Application")
check = "y"
def Gchordfunc(chord):
  if chord is "1":
    chord = "G"
    print("I in key of G is "+chord)
  elif chord is "2":
    chord = "Am"
    print("II in key of G is "+chord)
  elif chord is "3":
    chord = "Bm"
    print("III in key of G is "+chord)  
  elif chord is "4":
    chord = "C"
    print("IV in key of G is "+chord)
  elif chord is "5":
    chord = "D"
    print("V in key of G is "+chord)
  elif chord is "6":
    chord = "Em"
    print("VI in key of G is "+chord)
  elif chord is "7":
    chord = "D/F#"
    print("VII in key of G is "+chord)
    return chord




def Achordfunc(chord):
  if chord is "1":
    chord = "A"
    print("I in key of A is "+chord)
  elif chord is "2":
    chord = "Bm"
    print("II in key of A is "+chord)
  elif chord is "3":
    chord = "C#m"
    print("III in key of A is "+chord)  
  elif chord is "4":
    chord = "D"
    print("IV in key of A is "+chord)
  elif chord is "5":
    chord = "E"
    print("V in key of A is "+chord)
  elif chord is "6":
    chord = "F#m"
    print("VI in key of A is "+chord)
  elif chord is "7":
    chord = "E/G#"
    print("VII in key of A is "+chord)
    return chord


while check is "y":
  key= input("Input Key : ") 
  chord= input("Input Chord Progression : ") 
  if key is "g":
    Gchordfunc(chord);
  elif key is "a":
    Achordfunc(chord); 
  check = input("Press y to try it again? ")
